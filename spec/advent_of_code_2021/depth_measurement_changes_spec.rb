# frozen_string_literal: true

RSpec.describe AdventOfCode2021::DepthMeasurementChanges do
  subject { described_class.new(input) }

  describe '#part1' do
    let(:input) do
      <<~INPUT
        199
        200
        208
        210
        200
        207
        240
        269
        260
        263
      INPUT
    end

    it 'counts number of times depth measurement increases' do
      expect(subject.part1).to eq(7)
    end
  end

  describe '#part2' do
    let(:input) do
      <<~INPUT
        199
        200
        208
        210
        200
        207
        240
        269
        260
        263
      INPUT
    end

    it 'counts number of times depth measurement increases using a sliding window' do
      expect(subject.part2).to eq(5)
    end
  end
end
