# frozen_string_literal: true

require_relative './advent_of_code_2021/config'
require_relative './advent_of_code_2021/depth_measurement_changes'
require_relative './advent_of_code_2021/input'
require_relative './advent_of_code_2021/solver'
require_relative './advent_of_code_2021/version'

module AdventOfCode2021
  class Error < StandardError; end
  # Your code goes here...
end
