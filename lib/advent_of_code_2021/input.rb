# frozen_string_literal: true

require 'faraday'

module AdventOfCode2021
  class Input
    attr_reader :day

    def initialize(day: 1)
      @day = day
    end

    def fetch
      response = Faraday.get(uri, {}, headers)
      response.body
    end

    private

    def uri
      "https://adventofcode.com/2021/day/#{day}/input"
    end

    def headers
      { 'Cookie' => "session=#{Config.cookie}" }
    end
  end
end
