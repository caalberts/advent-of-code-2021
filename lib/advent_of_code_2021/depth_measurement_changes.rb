# frozen_string_literal: true

module AdventOfCode2021
  class DepthMeasurementChanges
    def initialize(input)
      @input = input.split("\n").lazy.map(&:to_i)
    end

    def part1
      measurements = @input

      count_increases(measurements)
    end

    def part2
      measurements = @input

      windowed_measurements = measurements.each_cons(3).map do |window|
        window.sum
      end

      count_increases(windowed_measurements)
    end

    private

    def count_increases(measurements)
      measurements.each_cons(2).inject(0) do |result, (a, b)|
        b > a ? result + 1 : result
      end
    end

    def parse_input(input)
      input.split("\n").map(&:to_i)
    end
  end
end
