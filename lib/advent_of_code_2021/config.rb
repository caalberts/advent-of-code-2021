# frozen_string_literal: true

module AdventOfCode2021
  class Config
    def self.cookie
      ENV['ADVENT_OF_CODE_2021_COOKIE']
    end
  end
end
