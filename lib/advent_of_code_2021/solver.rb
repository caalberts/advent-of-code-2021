# frozen_string_literal: true

module AdventOfCode2021
  class Solver
    PROBLEMS = {
      1 => DepthMeasurementChanges
    }.freeze

    attr_reader :day, :part

    def initialize(day: 1, part: 1)
      @day = day
      @part = part
    end

    def solve
      PROBLEMS.fetch(day).new(input).public_send("part#{part}")
    rescue KeyError
      warn "Unimplemented problem for day #{day}"
    end

    def verify_cookie
      Config.cookie || raise('Cookie required to authenticate with Advent of Code 2021')
    end

    def input
      verify_cookie
      AdventOfCode2021::Input.new(day: day).fetch
    end
  end
end
